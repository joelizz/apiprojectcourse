package br.ra.almeida.rest.tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.ra.almeida.rest.core.BaseTest;
import br.ra.almeida.rest.utils.DataUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BarrigaTest extends BaseTest{
	
	private String TOKEN;
	private static String CONTA_NAME = "Conta" + System.nanoTime();
	private static Integer CONTA_ID;
	private static Integer MOV_ID; 
	
	@Before
	public void login() {
		Map<String, String> login = new HashMap<>();
		login.put("email", "rafael@almeida.com");
		login.put("senha","123456");
		
		TOKEN = given()
		     .body(login)
		   .when()
		      .post("/signin")
		   .then()
		        .statusCode(200)
		        .extract().path("token");
		   ;
	}
	@Test
	public void t01_naoDeveAcessarAPISemToken() {
		given()
		   .when()
		      .get("/contas")
		   
		   .then()
		        .statusCode(401)
		   ;
	}
	@Test
	public void t02_DeveIncluirContaComSucesso() {
		   CONTA_ID = given()
		        .header("Authorization", "JWT " + TOKEN)
		       .body("{\"nome\": \""+CONTA_NAME+"\"}")
			.when()
			   .post("/contas")
		    .then()
		        .statusCode(201)
		        .extract().path("id")
				   ;
	}
	
	@Test
	public void t03_DeveAlterarContaComSucesso() {
		   given()
		        .header("Authorization", "JWT " + TOKEN)
		       .body("{\"nome\": \""+CONTA_NAME+" alterada\"}")
		       .pathParam("id", CONTA_ID)
			.when()
			   .put("/contas/{id}")
		    .then()
		        .statusCode(200)
		        .body("nome", is(CONTA_NAME+" alterada"))
				   ;
	}
	
	@Test
	public void t04_NaoDeveInserirContaComMesmoNome() {
		   given()
		        .header("Authorization", "JWT " + TOKEN)
		       .body("{\"nome\": \""+CONTA_NAME+" alterada\"}")
			.when()
			   .post("/contas")
		    .then()
		         .log().all()
		        .statusCode(400)
		        .body("error", is("J� existe uma conta com esse nome!"))
				   ;
	}

	@Test
	public void t05_DeveInserirMovimentacao() {
		Movimentacao mov = getMovimentacaoValida();
		
		  MOV_ID = given()
		        .header("Authorization", "JWT " + TOKEN)
		       .body(mov)
			.when()
			   .post("/transacoes")
		    .then()
		         .log().all()
		        .statusCode(201) 
		        .extract().path("id")
				   ;
	}
	
	 @Test
	 public void  t06_deveValidarCamposObrigatoriosMovimentacao() {
		 given()
	        .header("Authorization", "JWT " + TOKEN)
	       .body("{}")
		.when()
		   .post("/transacoes")
	    .then()
	         .log().all()
	        .statusCode(400) 
			.body("$", hasSize(8))
	        .body("msg", hasItems(
	        		"Data da Movimenta��o � obrigat�rio",
	        		"Data do pagamento � obrigat�rio",
	        		"Descri��o � obrigat�rio",
	        		"Interessado � obrigat�rio",
	        		"Valor � obrigat�rio",
	        		"Valor deve ser um n�mero",
	        		"Conta � obrigat�rio",
	        		"Situa��o � obrigat�rio"
	        		))
			;
	 }
	 @Test
	 public void  t07_naoDeveInserirMovimentacaoComDataFutura() {
		 Movimentacao mov = getMovimentacaoValida();
		 mov.setData_transacao("20/05/2022");
		 
		 given()
	        .header("Authorization", "JWT " + TOKEN)
	       .body(mov)
		.when()
		   .post("/transacoes")
	    .then()
	         .log().all()
	        .statusCode(400) 
	        .body("$", hasSize(1))
	        .body("msg",hasItem("Data da Movimenta��o deve ser menor ou igual � data atual"))
			;
	 }
	 
	 @Test
	 public void  t08_naoDeveRemoverContaComMovimentacao() {
		 given()
	        .header("Authorization", "JWT " + TOKEN)
	        .pathParam("id", CONTA_ID)
		.when()
		   .delete("/contas/{id}")
	    .then()
	        .statusCode(500)
	        .body("constraint",is("transacoes_conta_id_foreign"))
			;
	 }
	 
	 @Test
	 public void  t09_CalcularSoma() {
		 given()
	        .header("Authorization", "JWT " + TOKEN)
		.when()
		   .get("/saldo")
	    .then()
	        .statusCode(200)
	        .body("find{it.conta_id == "+CONTA_ID+"}.saldo", is("100.00"))
			;
	 }
	 //1022101
	 
	 @Test
	 public void  t10_RemoverMovimentacao() {
		 given()
	        .header("Authorization", "JWT " + TOKEN)
	        .pathParam("id",  MOV_ID)
		.when()
		   .delete("/transacoes/{id}")
	    .then()
	        .statusCode(204)
			;
	 }
	 
	 
	 private Movimentacao getMovimentacaoValida() {
			Movimentacao mov = new Movimentacao();
			mov.setConta_id(CONTA_ID);
			mov.setDescricao("Descricao da movimentacao");
			mov.setEnvolvido("Envolvido na mov");
			mov.setTipo("REC");
			mov.setData_transacao(DataUtils.getDataDiferencaDias(-1));
			mov.setData_pagamento(DataUtils.getDataDiferencaDias(5));
			mov.setValor(100f);
			mov.setStatus(true);
			return mov;
	 }
	 
	 
	
}
